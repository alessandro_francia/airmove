/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import MapView from 'react-native-maps';
import LinearGradient from 'react-native-linear-gradient';


export default class AirMove extends Component {
    state = {
        mapRegion: null,
        lastLat: null,
        lastLong: null,
        measurements: [],
        currentMarker: [
            {title: 'NO3', value: this.getRandomInRange(1, 10, 2)},
            {title: 'O3', value: this.getRandomInRange(1, 10, 2)},
            {title: 'PM10', value: this.getRandomInRange(1, 10, 2)},
            {title: 'PM25', value: this.getRandomInRange(1, 10, 2)}
        ]
    };

    getRandomInRange(from, to, fixed) {
        return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
        // .toFixed() returns string, so ' * 1' is a trick to convert to number
    }

    generateRandomMeasurements() {

        var measurements = [];

        for (let i = 0; i < 35; i++) {
            measurements.push(
                {
                    LatLng: {
                        latitude: this.getRandomInRange(51.649782, 51.335927, 10),
                        longitude: this.getRandomInRange(-0.378115, 0.49453, 10)
                    },
                    radius: 550,
                    status: [
                        {title: 'NO3', value: this.getRandomInRange(1, 10, 2)},
                        {title: 'O3', value: this.getRandomInRange(1, 10, 2)},
                        {title: 'PM10', value: this.getRandomInRange(1, 10, 2)},
                        {title: 'PM25', value: this.getRandomInRange(1, 10, 2)}
                    ],

                })
        }
        this.setState({measurements: measurements});
    }

    componentDidMount() {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            // Create the object to update this.state.mapRegion through the onRegionChange function
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            };
            this.onRegionChange(region, region.latitude, region.longitude);
        });
        this.generateRandomMeasurements();


    }

    onRegionChange(region, lastLat, lastLong) {
        this.setState({
            mapRegion: region,
            // If there are no new values set use the the current ones
            lastLat: lastLat || this.state.lastLat,
            lastLong: lastLong || this.state.lastLong
        });
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);

    }

    getBarWidth(value) {
        return 150 * (value / 10)
    }

    // onMapPress(e) {
    //     console.log(e.nativeEvent.coordinate.longitude);
    //     let region = {
    //         latitude:       e.nativeEvent.coordinate.latitude,
    //         longitude:      e.nativeEvent.coordinate.longitude,
    //         latitudeDelta:  0.00922*1.5,
    //         longitudeDelta: 0.00421*1.5
    //     }
    //     this.onRegionChange(region, region.latitude, region.longitude);
    // }


    // misurament = [
    //     {   //RED
    //         LatLng: {latitude: 51.509058, longitude: 0.040260},
    //         radius: 350,
    //         status: [
    //             {title: 'NO3', value: 1},
    //             {title: 'O3', value: 2},
    //             {title: 'PM10', value: 6.5},
    //             {title: 'PM25', value: 4},
    //         ],
    //     },
    //     {
    //         LatLng: {latitude: 51.510413, longitude: 0.033855},
    //         radius: 350,
    //         status: [
    //             {title: 'NO3', value: 7},
    //             {title: 'O3', value: 6.5},
    //             {title: 'PM10', value: 7},
    //             {title: 'PM25', value: 1},
    //         ],
    //
    //     }
    // ];
    misurament = [];

    calculateOverallScore(status) {
        let average = 0;
        status.forEach(function (stat) {
            average += stat.value;
        });
        average = average / status.length;
        return average;
    }

    getColor(value, alpha) {
        alpha = alpha || 1;
        if (value < 4)
            return `rgba(239, 87, 82, ${alpha})`;
        else if (value < 7)
            return `rgba(255, 184, 58, ${alpha})`;
        else
            return `rgba(122, 183, 51, ${alpha})`;
    }

    showMarkerDetail(marker) {
        // alert(JSON.stringify(marker))
        this.setState({currentMarker: marker.status})
    }

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>

                <MapView
                    // style={styles.map}
                    style={{flex: 5,}}
                    zoomEnabled={true}
                    region={this.state.mapRegion}
                    showsUserLocation={true}
                    followUserLocation={true}
                    onRegionChange={this.onRegionChange.bind(this)}
                    // onPress={this.onMapPress.bind(this)}
                >

                    {this.state.measurements.map((marker, i) => (
                        <MapView.Marker
                            key={i}
                            coordinate={marker.LatLng}
                            // radius={marker.radius}
                            pinColor={this.getColor(this.calculateOverallScore(marker.status), 0.5)}
                            strokeColor={this.getColor(this.calculateOverallScore(marker.status), 0.5)}
                            zIndex={2}
                            strokeWidth={2}
                            // onPress={this.showMarkerDetail}
                            onPress={(e) => {
                                e.stopPropagation();
                                this.showMarkerDetail(marker)
                            }}
                        />)
                    )}


                </MapView>
                <View style={{flex: 2, height: 50, backgroundColor: '#3f6566'}}>

                    <Text style={{
                        flex: 0.15, color: 'white', 'fontFamily': 'RockoFLF', fontSize : 28, marginTop :5, marginLeft:150
                    }}>AirMove </Text>


                    <View style={{flex: 0.85, flexDirection: 'row'}}>
                        <View style={{flex: 5, flexDirection: 'column', justifyContent: 'center'}}>

                            <View style={{height: 30, marginBottom: 2}}>
                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center',alignItems:'center'}}>
                                    <Text style={[styles.label, {width: 60}]}>NO

                                        <Text style={[styles.pedix]}>
                                            3
                                        </Text>
                                    </Text>

                                    <View style={{
                                        width: 150,
                                        height: 15,
                                        backgroundColor: '#e4e4e4',
                                        borderRadius: 5,
                                        borderWidth: 0,
                                        borderColor: '#fff',
                                        
                                    }}>
                                        <View style={{
                                            height: 15,
                                            width: this.getBarWidth(this.state.currentMarker[0].value),
                                            borderRadius: 5,
                                            backgroundColor: this.getColor(this.state.currentMarker[0].value),
                                            borderWidth: 0,
                                            borderColor: this.getColor(this.state.currentMarker[0].value),
                                            zIndex: 100
                                        }}/>

                                    </View>

                                </View>
                            </View>


                            <View style={{height: 30, marginBottom: 2}}>
                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center',alignItems:'center'}}>
                                    <Text style={[styles.label, {width: 60}]}>0

                                        <Text style={[styles.pedix]}>
                                            3
                                        </Text>
                                    </Text>

                                    <View style={{
                                        width: 150,
                                        height: 15,
                                        backgroundColor: '#e4e4e4',
                                        borderRadius: 5,
                                        borderWidth: 0,
                                        borderColor: '#fff',
                                        
                                    }}>
                                        <View style={{
                                            height: 15,
                                            width: this.getBarWidth(this.state.currentMarker[1].value),
                                            borderRadius: 5,
                                            backgroundColor: this.getColor(this.state.currentMarker[1].value),
                                            borderWidth: 0,
                                            borderColor: this.getColor(this.state.currentMarker[1].value),
                                            zIndex: 100
                                        }}/>

                                    </View>

                                </View>
                            </View>

                            <View style={{height: 30, marginBottom: 2}}>
                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center',alignItems:'center'}}>
                                    <Text style={[styles.label, {width: 60}]}>PM

                                        <Text style={[styles.pedix]}>
                                            10
                                        </Text>
                                    </Text>

                                    <View style={{
                                        width: 150,
                                        height: 15,
                                        backgroundColor: '#e4e4e4',
                                        borderRadius: 5,
                                        borderWidth: 0,
                                        borderColor: '#fff',
                                        
                                    }}>
                                        <View style={{
                                            height: 15,
                                            width: this.getBarWidth(this.state.currentMarker[2].value),
                                            borderRadius: 5,
                                            backgroundColor: this.getColor(this.state.currentMarker[2].value),
                                            borderWidth: 0,
                                            borderColor: this.getColor(this.state.currentMarker[2].value),
                                            zIndex: 100
                                        }}/>

                                    </View>

                                </View>
                            </View>
                            <View style={{height: 30, marginBottom: 2}}>
                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center',alignItems:'center'}}>
                                    <Text style={[styles.label, {width: 60}]}>PM

                                        <Text style={[styles.pedix]}>
                                            25
                                        </Text>
                                    </Text>

                                    <View style={{
                                        width: 150,
                                        height: 15,
                                        backgroundColor: '#e4e4e4',
                                        borderRadius: 5,
                                        borderWidth: 0,
                                        borderColor: '#fff'
                                    }}>
                                        <View style={{
                                            height: 15,
                                            width: this.getBarWidth(this.state.currentMarker[3].value),
                                            borderRadius: 5,
                                            backgroundColor: this.getColor(this.state.currentMarker[3].value),
                                            borderWidth: 0,
                                            borderColor: this.getColor(this.state.currentMarker[3].value),
                                            zIndex: 100
                                        }}/>

                                    </View>

                                </View>
                            </View>


                        </View>
                        <View style={{flex: 2, alignItems: 'center'}}>

                            <View style={{
                                marginTop: 5,
                                width: 30,
                                height: 150,
                                backgroundColor: '#e4e4e4',
                                borderRadius: 5,
                                borderWidth: 0,
                                borderColor: '#fff',

                            }}>
                                <View style={{
                                    marginTop: 150 - this.getBarWidth(this.calculateOverallScore(this.state.currentMarker)),
                                    height: this.getBarWidth(this.calculateOverallScore(this.state.currentMarker)),
                                    width: 30,
                                    borderRadius: 5,
                                    bottom: 0,

                                    backgroundColor: this.getColor(this.calculateOverallScore(this.state.currentMarker)),
                                    borderWidth: 0,
                                    borderColor: this.getColor(this.calculateOverallScore(this.state.currentMarker)),
                                    zIndex: 100
                                }}/>

                            </View>

                        </View>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    firstLabel: {
        marginTop: 50
    },
    firstLabelPedix: {
        marginTop: 63
    },
    label: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#cccccc',
        marginLeft: 40,
        marginRight :5
    },
    pedix: {
        fontWeight: 'bold',
        marginLeft: 0,
        marginTop: 5,
        fontSize: 16,


    }
});


AppRegistry.registerComponent('AirMove', () => AirMove);
